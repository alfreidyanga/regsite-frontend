import { wrapper } from "../src/redux/store";

function MyApp({ Component, pageProps }) {
  return (
    <div>
      <style jsx global>{`
        body {
          background-image: linear-gradient(
            to top,
            #e5f891,
            #5de7a4,
            #00cbd6,
            #00a6fc,
            #1271eb
          );
          margin: auto;
        }
      `}</style>
      <Component {...pageProps} />
    </div>
  );
}

export default wrapper.withRedux(MyApp);
