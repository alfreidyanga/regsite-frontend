import { Avatar, Box, Card, styled, Typography } from "@mui/material";
import { userLogout } from "../../src/redux/actions";
import { useDispatch } from "react-redux";
import { useRouter } from "next/router";
import { BASE_URL } from "../../config";
import React, { useEffect, useState } from "react";

const StyledCard = styled(Card)`
  padding: 2rem 3rem 2rem 3rem;
  @media (min-width: 768px) {
    width: 500px;
  }
`;

export default function Main() {
  const dispatch = useDispatch();
  const router = useRouter();
  const [user, setUser] = useState({});
  useEffect(() => {
    if (window.localStorage.getItem("user") === null) {
      router.push("/login");
    } else {
      let userDetails = JSON.parse(window.localStorage.getItem("user"));
      if (userDetails.is_complete === true) {
        setUser(JSON.parse(window.localStorage.getItem("user")));
      } else {
        router.push("/complete");
      }
    }
  }, []);

  const Logout = () => {
    dispatch(userLogout()).then((res) => {
      if (res != undefined) {
        router.push("/login");
      }
    });
  };

  return (
    <Box
      display="flex"
      flexDirection="column"
      minHeight="100vh"
      alignItems="center"
      justifyContent="center"
    >
      {user && (
        <StyledCard elevation={3}>
          <Typography variant="body1" textAlign="center" mb={2.5}>
            Hello {user.username}! These are your user inputs for the steps.
          </Typography>
          <div>
            {user.profile_picture && (
              <Avatar
                src={BASE_URL + user.profile_picture}
                sx={{
                  height: 64,
                  width: 64,
                  display: "flex",
                  margin: "auto",
                }}
              />
            )}
          </div>
          <Typography variant="body1" textAlign="center" mb={2.5}>
            {user.first_name} {user.last_name}
          </Typography>
          <Typography variant="body1" textAlign="center">
            Email: {user.email}
          </Typography>
          <Typography variant="body1" textAlign="center">
            Date of Birth: {user.date_of_birth}
          </Typography>
          <Typography variant="body1" textAlign="center">
            Contact Number: {user.contact_number}
          </Typography>
          <Box mt="1rem" onClick={Logout} style={{ cursor: "pointer" }}>
            <Typography variant="body1" textAlign="center">
              <u>Logout</u>
            </Typography>
          </Box>
        </StyledCard>
      )}
    </Box>
  );
}
