import {
  Box,
  Button,
  Card,
  Paper,
  styled,
  TextField,
  Typography,
} from "@mui/material";
import { userLogin } from "../../src/redux/actions";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { useRouter } from "next/router";
import Link from "next/link";
import React, { useEffect, useState } from "react";

const ErrorMessage = styled(Paper)`
  && {
    background-color: red;
    color: white;
    padding: 10px;
    margin: 10px 0 10px 0;
    text-align: center;
  }
`;

const StyledCard = styled(Card)`
  padding: 2rem 3rem 2rem 3rem;
  @media (min-width: 768px) {
    width: 500px;
  }
`;

const StyledTextField = styled(TextField)`
  margin: 0.5rem 0 0.5rem 0;
`;

const Login = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const dispatch = useDispatch();
  const router = useRouter();
  const [errorMessage, setErrorMessage] = useState("");

  useEffect(() => {
    if (window.localStorage.getItem("user") != null) {
      let userDetails = JSON.parse(window.localStorage.getItem("user"));
      if (userDetails.is_complete === true) {
        router.push("/main");
      } else {
        router.push("/complete");
      }
    }
  }, []);

  const onFormSubmit = (formData) => {
    dispatch(userLogin(formData)).then((res) => {
      if (res != undefined) {
        if (res.data.error_message) {
          setErrorMessage(res.data.error_message);
        } else {
          if (res.data.is_complete === true) {
            router.push("/main");
          } else {
            router.push("/complete");
          }
        }
      }
    });
  };

  return (
    <Box
      display="flex"
      flexDirection="column"
      minHeight="100vh"
      alignItems="center"
      justifyContent="center"
    >
      <StyledCard elevation={3}>
        <form onSubmit={handleSubmit(onFormSubmit)}>
          <Typography variant="h5" textAlign="center" mb={2}>
            Login
          </Typography>
          <StyledTextField
            fullWidth
            type="text"
            label="Username"
            size="small"
            variant="outlined"
            placeholder="Type your username"
            InputLabelProps={{
              shrink: true,
            }}
            {...register("username", {
              required: "You must provide your username!",
            })}
            error={!!errors.username}
            helperText={!!errors.username ? errors.username.message : " "}
          />
          <StyledTextField
            fullWidth
            type="password"
            label="Password"
            size="small"
            variant="outlined"
            placeholder="Type your password"
            InputLabelProps={{
              shrink: true,
            }}
            {...register("password", {
              required: "You must provide your password!",
            })}
            error={!!errors.password}
            helperText={!!errors.password ? errors.password.message : " "}
          />
          {errorMessage && (
            <ErrorMessage>
              <Typography variant="body2"> {errorMessage} </Typography>
            </ErrorMessage>
          )}
          <Button fullWidth type="submit" variant="contained">
            <Typography variant="body2">Login</Typography>
          </Button>
        </form>
        <Box mt="1rem">
          <Typography variant="body1" textAlign="center">
            Don&rsquo;t have an account? &nbsp;
            <Link href="/signup">Signup</Link>
          </Typography>
        </Box>
      </StyledCard>
    </Box>
  );
};

export default Login;
