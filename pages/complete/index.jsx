import StepParent from "../../src/components/StepParent";
import { FormProvider } from "../../src/redux/reducers";
import React from "react";

const Complete = () => {
  return (
    <FormProvider>
      <StepParent />
    </FormProvider>
  );
};

export default Complete;
