import Head from "next/head";

export default function Home() {
  return (
    <Head>
      <title>Registration</title>
      <meta name="description" content="Simple registration website" />
      <link rel="icon" href="/favicon.ico" />
    </Head>
  );
}
