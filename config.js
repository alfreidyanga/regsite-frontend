module.exports = {
  // for development
  // API_SERVER: "http://127.0.0.1:8000/api",
  // BASE_URL: "http://127.0.0.1:8000",

  // for production
  API_SERVER:
    "https://alfreidyanga.pythonanywhere.com/api",
  BASE_URL:
    "https://alfreidyanga.pythonanywhere.com",
};
