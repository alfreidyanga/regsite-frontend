import {
  userSignupAPI,
  userLoginAPI,
  userLogoutAPI,
  userCompleteAPI,
} from "./api";

export const userSignup = (data) => async (dispatch) => {
  let response = await userSignupAPI(data);
  if (!response.data.error_message) {
    window.localStorage.setItem("user", JSON.stringify(response.data));
  }
  return response;
};

export const userLogin = (data) => async (dispatch) => {
  let response = await userLoginAPI(data);
  if (!response.data.error_message) {
    window.localStorage.setItem("user", JSON.stringify(response.data));
  }
  return response;
};

export const userLogout = () => async (dispatch) => {
  let response = await userLogoutAPI();
  window.localStorage.removeItem("user");
  return response;
};

export const userComplete = (data) => async (dispatch) => {
  let response = await userCompleteAPI(data);
  window.localStorage.setItem("user", JSON.stringify(response.data));
  return response;
};
