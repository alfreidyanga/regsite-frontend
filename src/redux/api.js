import axios from "axios";
import Config from "../../config";

export const userSignupAPI = (data) => {
  return axios.post(`${Config.API_SERVER}/signup`, data);
};

export const userLoginAPI = (data) => {
  return axios.post(`${Config.API_SERVER}/login`, data);
};

export const userLogoutAPI = () => {
  return axios.post(`${Config.API_SERVER}/logout`);
};

export const userCompleteAPI = (data) => {
  return axios.post(`${Config.API_SERVER}/complete`, data);
};
