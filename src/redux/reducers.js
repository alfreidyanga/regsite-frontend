import React, { createContext, useReducer, useContext } from "react";
import { combineReducers } from "redux";

function formReducer(state = initialState, action) {
  switch (action.type) {
    case "FIRST_NAME_CHANGE":
      return { ...state, first_name: action.payload };
    case "LAST_NAME_CHANGE":
      return { ...state, last_name: action.payload };
    case "EMAIL_CHANGE":
      return { ...state, email: action.payload };
    case "DATE_OF_BIRTH_CHANGE":
      return { ...state, date_of_birth: action.payload };
    case "CONTACT_NUMBER_CHANGE":
      return { ...state, contact_number: action.payload };
    case "PROFILE_PICTURE_CHANGE":
      return { ...state, profile_picture: action.payload };
    default:
      return state;
  }
}

const FormContext = createContext();

const initialState = {
  first_name: "",
  last_name: "",
  email: "",
  date_of_birth: "",
  contact_number: "",
  profile_picture: null,
};

export const FormProvider = function ({ children }) {
  const [state, dispatch] = useReducer(formReducer, initialState);

  return (
    <FormContext.Provider value={{ state, dispatch }}>
      {children}
    </FormContext.Provider>
  );
};

export function useFormState() {
  const context = useContext(FormContext);

  if (context === undefined) {
    throw new Error("useFormState must be used within a FormProvider");
  }

  return context;
}

export default combineReducers({ form: formReducer });
