import { Button, Grid, styled, TextField } from "@mui/material";
import { useFormState } from "../../redux/reducers";
import { useForm } from "react-hook-form";
import React from "react";

const StyledTextField = styled(TextField)`
  margin: 0.5rem 0 0.5rem 0;
`;

const Step1 = ({ nextStep }) => {
  const { state } = useFormState();
  const {
    state: { first_name, last_name, email },
    dispatch,
  } = useFormState();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onFormSubmit = () => {
    nextStep();
  };

  return (
    <form onSubmit={handleSubmit(onFormSubmit)}>
      <Grid container>
        <Grid item xs={5.5}>
          <StyledTextField
            fullWidth
            type="text"
            label="First Name"
            size="small"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
            }}
            {...register("first_name", {
              required: "You must provide your first name!",
            })}
            error={!!errors.first_name}
            helperText={!!errors.first_name ? errors.first_name.message : " "}
            value={first_name}
            onChange={(e) =>
              dispatch({
                type: "FIRST_NAME_CHANGE",
                payload: e.target.value,
              })
            }
          />
        </Grid>
        <Grid item xs={1} />
        <Grid item xs={5.5}>
          <StyledTextField
            fullWidth
            type="text"
            label="Last Name"
            size="small"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
            }}
            {...register("last_name", {
              required: "You must provide your first name!",
            })}
            error={!!errors.last_name}
            helperText={!!errors.last_name ? errors.last_name.message : " "}
            value={last_name}
            onChange={(e) =>
              dispatch({
                type: "LAST_NAME_CHANGE",
                payload: e.target.value,
              })
            }
          />
        </Grid>
        <Grid item xs={12}>
          <StyledTextField
            fullWidth
            type="email"
            label="Email"
            size="small"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
            }}
            {...register("email", {
              required: "You must provide your email!",
            })}
            error={!!errors.email}
            helperText={!!errors.email ? errors.email.message : " "}
            value={email}
            onChange={(e) =>
              dispatch({
                type: "EMAIL_CHANGE",
                payload: e.target.value,
              })
            }
          />
        </Grid>
        <Grid item xs={6.5} />
        <Grid item xs={5.5}>
          <Button fullWidth type="submit" variant="contained">
            Next
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default Step1;
