import { Button, Grid, styled, TextField } from "@mui/material";
import { useFormState } from "../../redux/reducers";
import { useForm } from "react-hook-form";
import React from "react";

const StyledTextField = styled(TextField)`
  margin: 0.5rem 0 0.5rem 0;
`;

const Step3 = ({ prevStep, completeUser }) => {
  const { state } = useFormState();
  const {
    state: { profile_picture },
    dispatch,
  } = useFormState();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onBackClick = (e) => {
    e.preventDefault;
    prevStep();
  };

  const onFormSubmit = () => {
    completeUser();
  };

  return (
    <form onSubmit={handleSubmit(onFormSubmit)}>
      <Grid container>
        <Grid item xs={12}>
          <StyledTextField
            fullWidth
            type="file"
            size="small"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
            }}
            InputProps={{ inputProps: { accept: "image/png, image/jpeg" } }}
            {...register("profile_picture", {
              required: "You must provide your profile picture!",
            })}
            error={!!errors.profile_picture}
            helperText={
              !!errors.profile_picture
                ? errors.profile_picture.message
                : "Upload profile picture"
            }
            onChange={(e) =>
              dispatch({
                type: "PROFILE_PICTURE_CHANGE",
                payload: e.currentTarget.files[0],
              })
            }
          />
        </Grid>
        <Grid item xs={5.5}>
          <Button fullWidth onClick={onBackClick} variant="contained">
            Back
          </Button>
        </Grid>
        <Grid item xs={1} />
        <Grid item xs={5.5}>
          <Button fullWidth type="submit" variant="contained">
            Submit
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default Step3;
