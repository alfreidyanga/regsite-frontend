import Step1 from "../Step1";
import Step2 from "../Step2";
import Step3 from "../Step3";
import {
  Box,
  Card,
  Step,
  StepLabel,
  Stepper,
  styled,
  Typography,
} from "@mui/material";
import { userLogout, userComplete } from "../../redux/actions";
import { useFormState } from "../../redux/reducers";
import { useDispatch } from "react-redux";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";

const StyledCard = styled(Card)`
  padding: 2rem 3rem 2rem 3rem;
  @media (min-width: 768px) {
    width: 500px;
  }
`;

const steps = ["Step 1", "Step 2", "Step 3"];

const StepParent = () => {
  const { state } = useFormState();
  const dispatch = useDispatch();
  const router = useRouter();
  const [step, setStep] = useState(1);

  useEffect(() => {
    if (window.localStorage.getItem("user") != null) {
      let userDetails = JSON.parse(window.localStorage.getItem("user"));
      if (userDetails.is_complete === true) {
        router.push("/main");
      }
    } else {
      router.push("/login");
    }
  }, []);

  const Logout = () => {
    dispatch(userLogout()).then((res) => {
      if (res != undefined) {
        router.push("/login");
      }
    });
  };

  const prevStep = () => {
    setStep(step - 1);
  };

  const nextStep = () => {
    setStep(step + 1);
  };

  const completeUser = () => {
    const user_id = JSON.parse(window.localStorage.getItem("user")).id;
    let formData = new FormData();
    let formKeys = Object.keys(state);
    let formValues = Object.values(state);
    formData.append("user_id", user_id);

    for (let i = 0; i < formKeys.length; i++) {
      formData.append(formKeys[i], formValues[i]);
    }

    dispatch(userComplete(formData)).then((res) => {
      if (res != undefined) {
        router.push("/main");
      }
    });
  };

  const renderSwitch = (step) => {
    switch (step) {
      case 1:
        return <Step1 nextStep={nextStep} />;
      case 2:
        return <Step2 nextStep={nextStep} prevStep={prevStep} />;
      case 3:
        return <Step3 prevStep={prevStep} completeUser={completeUser} />;
      default:
    }
  };

  return (
    <Box
      display="flex"
      flexDirection="column"
      minHeight="100vh"
      alignItems="center"
      justifyContent="center"
    >
      <StyledCard elevation={3}>
        <Box sx={{ width: "100%" }} mb={4}>
          <Stepper activeStep={step - 1} alternativeLabel>
            {steps.map((label) => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>
        </Box>
        {renderSwitch(step)}
        <Box mt="1rem" onClick={Logout} style={{ cursor: "pointer" }}>
          <Typography variant="body1" textAlign="center">
            <u>Logout</u>
          </Typography>
        </Box>
      </StyledCard>
    </Box>
  );
};

export default StepParent;
