import { Button, Grid, styled, TextField } from "@mui/material";
import { useFormState } from "../../redux/reducers";
import { useForm } from "react-hook-form";
import moment from "moment";
import React from "react";

const StyledTextField = styled(TextField)`
  margin: 0.5rem 0 0.5rem 0;
`;

const Step2 = ({ nextStep, prevStep }) => {
  const { state } = useFormState();
  const {
    state: { date_of_birth, contact_number },
    dispatch,
  } = useFormState();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onBackClick = (e) => {
    e.preventDefault;
    prevStep();
  };

  const onFormSubmit = () => {
    nextStep();
  };

  return (
    <form onSubmit={handleSubmit(onFormSubmit)}>
      <Grid container>
        <Grid item xs={12}>
          <StyledTextField
            fullWidth
            type="date"
            label="Date of Birth"
            size="small"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
            }}
            InputProps={{ inputProps: { max: moment().format("YYYY-MM-DD") } }}
            {...register("date_of_birth", {
              required: "You must provide your date of birth!",
            })}
            error={!!errors.date_of_birth}
            helperText={
              !!errors.date_of_birth ? errors.date_of_birth.message : " "
            }
            value={date_of_birth}
            onChange={(e) =>
              dispatch({
                type: "DATE_OF_BIRTH_CHANGE",
                payload: e.target.value,
              })
            }
          />
        </Grid>
        <Grid item xs={12}>
          <StyledTextField
            fullWidth
            type="text"
            label="Contact Number"
            size="small"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
            }}
            {...register("contact_number", {
              required: "You must provide your contact number!",
            })}
            error={!!errors.contact_number}
            helperText={
              !!errors.contact_number ? errors.contact_number.message : " "
            }
            value={contact_number}
            onChange={(e) =>
              dispatch({
                type: "CONTACT_NUMBER_CHANGE",
                payload: e.target.value,
              })
            }
          />
        </Grid>
        <Grid item xs={5.5}>
          <Button fullWidth onClick={onBackClick} variant="contained">
            Back
          </Button>
        </Grid>
        <Grid item xs={1} />
        <Grid item xs={5.5}>
          <Button fullWidth type="submit" variant="contained">
            Next
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default Step2;
